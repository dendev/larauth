<?php

use Dendev\Larauth\Http\Middleware\ChooseLoginUrl;
use Illuminate\Support\Facades\Route;
use Livewire\Volt\Volt;

// Override login
Volt::route('login', 'pages.auth.login')
    ->middleware(ChooseLoginUrl::class) // ignore
    ->name('login');

// Cas
Route::get('/cas/login', 'Dendev\Larauth\Http\Controllers\CasController@login')->name('cas.login');
Route::get('/cas/logout', 'Dendev\Larauth\Http\Controllers\CasController@logout')->name('cas.logout');

// Saml
Route::get('/saml2/metadata', '\Dendev\Larauth\Http\Middleware\AppSaml2@saml2metadata');

Route::get('/login-saml', '\Dendev\Larauth\Http\Middleware\AppSaml2@saml2')->name('saml.login');
Route::post('/login/saml2callback', '\Dendev\Larauth\Http\Middleware\AppSaml2@saml2callback');

Route::get('/logout-saml', '\Dendev\Larauth\Http\Middleware\AppSaml2@saml2logout')->name('saml.logout');
Route::post('/logout/saml2callback', '\Dendev\Larauth\Http\Middleware\AppSaml2@logoutcallback');
