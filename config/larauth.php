<?php

return [
    'homepage' => 'dashboard',
    'save_usage' => true,
    'default_duration' => 10,
    'notifications' => [
        'slack' => [
            'use_it' => true,
            // see services.php for config
        ],
        'teams' => [
            'use_it' => false,
        ],
        'mail' => [
            'use_it' => true,
            'to' => 'admin@test.com'
        ],
    ],

    'logging' => [
        'repository' => [
            'driver' => 'daily',
            'path' => storage_path('logs/larauth.log'),
            'level' => 'debug',
            'days' => 14,
        ],
    ],

    'user_full_namespace' => 'App\Models\User',
    'user_class' => 'User',

    'after_auth_full_namespace' => 'App\Utils\AfterAuth',
    'after_auth_class' => 'AfterAuth',
];
