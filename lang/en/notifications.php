<?php

return [
    'magic_password_created_subject' => 'Magic password created',
    'magic_password_created_hello' => 'Hi !',
    'magic_password_created_info' => 'A new magic password ( :magic_password_id ) is created with value :magic_password_value and expiration date :magic_password_expires_at',


];
