<?php

namespace Dendev\Larauth;

use Dendev\Larauth\Console\Commands\GenerateMagicPassword;
use Dendev\Larauth\Console\Commands\Install;
use Dendev\Larauth\Console\Commands\StopMagicPasswords;
use Dendev\Larauth\Http\Middleware\ChooseLoginUrl;
use Dendev\Larauth\Providers\EventServiceProvider;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class LarauthServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(Router $router): void
    {
        //$router->aliasMiddleware('choose.login.url', ChooseLoginUrl::class);
        $this->loadTranslationsFrom(__DIR__.'/../lang', 'dendev');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'dendev');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../routes.php');


        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__.'/../config/larauth.php', 'larauth');

        $this->mergeConfigFrom(__DIR__ . '/../config/services.php', 'services');

        $this->app->singleton('larauth', function ($app) {
            return new Larauth;
        });

        $this->app->register(EventServiceProvider::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['larauth'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole(): void
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/larauth.php' => config_path('larauth.php'),
        ], 'larauth.config');

        /*
        $this->publishes([
            __DIR__.'/../config/services.php' => config_path('services.php'),
        ], 'larauth.config');
        */

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/dendev'),
        ], 'larauth.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/dendev'),
        ], 'larauth.assets');*/

        // Publishing the translation files.
        $this->publishes([
            __DIR__.'/../lang' => resource_path('lang/vendor/dendev'),
        ], 'larauth.lang');

        // Registering package commands.
        $this->commands([
            Install::class,
            GenerateMagicPassword::class,
            StopMagicPasswords::class,
        ]);
    }
}
