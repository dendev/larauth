<?php

namespace Dendev\Larauth\Http\Middleware;
use App\Models\User;
use Closure;

use Illuminate\Support\Facades\Cookie;
use RootInc\LaravelSaml2Middleware\Saml2 as Saml2;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;


class AppSaml2 extends Saml2
{
    /**
     * Handler that is called when a successful login has taken place for the first time
     *
     * @param \Illuminate\Http\Request $request
     * @param String $token
     * @param mixed $profile
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|mixed
     */
    protected function success($request, $token, $profile)
    {
        $user_infos = [];

        // get remote datas
        $user_infos['userPrincipalName'] = $profile['userPrincipalName'][0];
        $user_infos['login'] = $profile['userPrincipalName'][0];
        $user_infos['email'] = $profile['userPrincipalName'][0];

        // make username
        $user_infos['username'] = $profile['userPrincipalName'][0];
        if( array_key_exists('lastname', $profile ) && array_key_exists('firstname', $profile))
            $user_infos['username'] = $profile['lastname'][0] . ' ' . $profile['firstname'][0];

        // create user
        $email = $user_infos['email'];
        $password = 'test'; //Hash::make(env('MAGIC_PASSWORD'));
        $user = User::updateOrCreate(
            ['email' => $email],
            [
                'name' => $user_infos['username'],
                'password' => $password,
            ]);
        Log::debug("[USER] Création de $email dans la base de données locale");

        // auth in laravel
        if( $user)
            Auth::login( $user );

        // log
        Log::info("[AppSaml2::success] ASs01 : user '{$user_infos['email']} log with WSO2 SAML",[
            'user_infos' => $user_infos
        ]);


        return redirect()->intended(route(config('larauth.homepage', 'dashboard')));
    }

    public function saml2logout(Request $request)
    {
        $auth = $this->getAuth();

        if( $auth->isAuthenticated() )
        {
            $request->session()->pull('_rootinc_saml2_id');
            return redirect()->away($this->getLogoutUrl());
        }
        else
        {
            return $this->logoutsuccess($request);
        }
    }

    /**
     * Callback after logout from Saml2
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|mixed
     * @throws \Exception
     */
    public function logoutcallback(\Illuminate\Http\Request $request)
    {
        $auth = $this->getAuth();

        if( ! $auth->isAuthenticated() )
        {
            // destroy the local session by firing the Logout event
            $keep_local_session = false;
            $retrieveParametersFromServer = false; //originally configurable in saml2_settings

            // WSO2 envoi du POST et processSLO fonctionne en GET uniquement
            if(!empty($_POST['SAMLResponse']))
            {
                $_GET['SAMLResponse'] = $_POST['SAMLResponse'];
            }

            if(!empty($_POST['SAMLRequest'])){
                $_GET['SAMLRequest'] = $_POST['SAMLRequest'];
            }

            $auth->processSLO($keep_local_session, null, $retrieveParametersFromServer);
            $errors = $auth->getErrors();

            if (!empty($errors))
            {
                $this->logoutfail($request, $errors);
            }

        }
        return $this->logoutsuccess($request);
    }

    /**
     * Handler that is called when a successful logout has taken place
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|mixed
     */
    protected function logoutsuccess(\Illuminate\Http\Request $request)
    {
        $request->session()->invalidate();

        \Cookie::forget('cas_session');
        \Cookie::forget('PHPSESSID');
        \Cookie::forget('XSRF-TOKEN');
        \Cookie::forget('portail_session');
        \Cookie::forget('MoodleSession');

        return redirect(env('SAML2_AFTER_LOGOUT_URL', $this->login_route));
    }
}

