<?php

namespace Dendev\Larauth\Http\Middleware;

use Closure;

class ChooseLoginUrl
{
    /**
     * Redirect to auth.henallux if option in .env is enabled
     * We can bypass auth with url /login?ignore
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( ( env('CAS_ENABLED' ) || env('SAML_ENABLED') ) && ! $request->exists('ignore'))
        {
            $prefix = ( env('CAS_ENABLED') ) ? 'cas'  : 'saml';

            if( $request->getRequestUri() == '/login' )
                return redirect()->route("$prefix.login");
            else if( $request->getRequestUri() == '/logout')
                return redirect()->route("$prefix.logout");
        }

        return $next($request);
    }
}
