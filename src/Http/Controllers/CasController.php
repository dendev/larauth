<?php

namespace Dendev\Larauth\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class CasController extends Controller
{
    protected $redirectTo = '/';
    public function login( Request $request )
    {
        if ($user_infos = $this->_attemptLoginCas())
        {
            $user_infos['login'] = $user_infos['userPrincipalName'];// unsername@mail.com
            $user_infos['email'] = $user_infos['userPrincipalName'];

            $user_infos['username'] = $user_infos['userPrincipalName'];
            if( array_key_exists('lastname', $user_infos ) && array_key_exists('firstname', $user_infos))
                $user_infos['username'] = $user_infos['lastname'] . ' ' . $user_infos['firstname'];
            else
                $user_infos['username'] = substr($user_infos['userPrincipalName'], 0, strpos($user_infos['userPrincipalName'], '@'));

            // create user && auth
            $email = $user_infos['email'];
            $password = 'test'; //Hash::make(env('MAGIC_PASSWORD'));
            $user = \App\Models\User::updateOrCreate(
                ['email' => $email],
                [
                    'name' => $user_infos['username'],
                    'password' => $password,
                ]);
            Log::debug("[USER] Création de $email dans la base de données locale");

            Auth::login($user);

            $after_auth_full_classname = config('larauth.after_auth_full_classname');
            if( class_exists($after_auth_full_classname))
                $after_auth = new $after_auth_full_classname($user_infos);

            dd( Auth::user());
            // auth in backpack
            //
            //if( $user->hasPermissionTo('backpack_access'))
              //  Auth::guard(backpack_guard_name())->login($user);

            // activity
            //\ActivityManager::log($user, "User logging by SAML", null, $user, 'user');


            // log
            \Log::stack(['auth', 'stack'])->info("[CasController::login] CCl01 : user '{$user_infos['email']} log with WSO2",[
                'user_infos' => $user_infos
            ]);
        }

//        $this->incrementLoginAttempts($request);

        //return redirect()->intended(RouteServiceProvider::HOME);
        return redirect()->intended(route('home'));
    }
    public function logout( Request $request )
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        \Cookie::forget('cas_session');
        \Cookie::forget('PHPSESSID');
        \Cookie::forget('XSRF-TOKEN');
        \Cookie::forget('inscription_session');
        \Cookie::forget('portail_session');
        \Cookie::forget('MoodleSession');

        \phpCAS::client(CAS_VERSION_2_0, env( 'CAS_HOSTNAME') , (int) env('CAS_PORT'), env( 'CAS_URI' ) );
        \phpCAS::logout(['service' => env('LOGOUT_GO_TO_URL')]);

        if ($request->wantsJson()) {
            return response()->json([], 200);
        }

        return true;
    }
    private function _attemptLoginCas()
    {
        $user_infos = false;

        \phpCAS::client(\CAS_VERSION_2_0, env( 'CAS_HOSTNAME') , (int) env('CAS_PORT'), env( 'CAS_URI' ), env('APP_URL') ); // FIXME 5 arg !? service base url

        \phpCAS::setFixedServiceURL(env('APP_URL') . '/cas/login');
        \phpCAS::setNoCasServerValidation(); // ! https://github.com/subfission/cas/blob/master/src/Subfission/Cas/CasManager.php Row 156
        if( ! \phpCAS::isAuthenticated() )
        {
            \phpCAS::forceAuthentication();
        }

        $user_infos = \phpCAS::getAttributes();
        if( empty( $user_infos ) )
        {
            Log::error("[LOGIN] Echec connexion CAS, pas d'infos utilisateurs");
            $ok = false;
        }
        else
        {
            $ok = true;
        }

        return $user_infos;
    }
}
