<?php
namespace Dendev\Larauth\Listeners;

use App\Models\User;
use Dendev\Larauth\Models\MagicPassword;
use Dendev\Larauth\Models\MagicPasswordUser;
use Illuminate\Auth\Events\Attempting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserAuthAttempting
{
    /**
     * Handle user login events.
     */
    public function handle(Attempting $event)
    {
        $email = $event->credentials['email'];
        $password = $event->credentials['password'];

        $magic_passwords = MagicPassword::where('expires_at',  '>=',  now())->get()->pluck('password', 'id');

        // check if match with one active magic password
        $used_magic_password_id = false;
        foreach( $magic_passwords as $id => $magic_password )
        {
            $is_checked = Hash::check($password, $magic_password);
            if( $is_checked )
            {
                $used_magic_password_id = $id;
                break;
            }
        }

        // magic password used  ?
        if( $used_magic_password_id)
        {
            // get user
            $user = User::where('email', $email)->first();
            if( ! $user )
            {
                $user = new User([
                    'name' => $email,
                    'email' => $email,
                    'password' => Hash::make(uniqid())
                ]);
                $user->save();
            }

            // save usage
            if( config('larauth.save_usage', true))
            {
                $used_magic_password = MagicPassword::find($used_magic_password_id);
                $magic_password_user = new MagicPasswordUser([
                    'user_id' => $user->id,
                    'magic_password_id' => $used_magic_password_id
                ]);
                $magic_password_user->save();
            }

            // log
            Log::warning("MAGIC AUTH: $user->email logged with magic password", [
                'user_id' => $user->id,
                'user_email' => $user->email,
                'magic_password_id' => $used_magic_password->id,
            ]);

            // auth in laravel
            Auth::login($user);

            // end
            return redirect()->intended(route(config('larauth.homepage', 'dashboard')));
        }
    }
}
