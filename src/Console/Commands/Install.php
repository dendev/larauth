<?php

namespace Dendev\Larauth\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Artisan;

class Install extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larauth:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install basic laravel auth';


    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->_install_breeze();

        $this->_inform();
    }

    private function _install_breeze()
    {
        $this->info("** Install Breeze");

        // init
        $output = null;
        $is_composer_breeze_installed = null;

        // breeze
        exec('composer require laravel/breeze --dev', $output, $is_composer_breeze_installed);
        $is_breeze_installed = Artisan::call('breeze:install', ['livewire']);

        // migrate
        $is_migrated = Artisan::call('migrate', []);

        // all is ok
        $is_installed = $is_composer_breeze_installed === 0 && $is_breeze_installed === 0  && $is_migrated === 0;

        // inform user
        if( $is_installed )
            $this->info("Ok installed");
        else
            $this->info("Ko not installed");

        // debug
        //dump( $is_composer_breeze_installed);
        //dump( $is_breeze_installed);
        //dump( $is_migrated);

        return $is_installed;
    }

    private function _inform(): void // TODO in trait
    {
        $this->info("** Info");
        $this->info("Go to " . env('APP_URL') . '/login');
        $this->info("\n");
    }
}

// refs :
