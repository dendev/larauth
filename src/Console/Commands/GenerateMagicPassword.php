<?php

namespace Dendev\Larauth\Console\Commands;

use App\Models\User;
use Dendev\Larauth\Models\MagicPassword;
use Dendev\Larauth\Notifications\MagicPasswordCreatedNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class GenerateMagicPassword extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larauth:generate_magic_password {--d|duration=10 : magic password duration limit} {--notif : send notification when magic password is created}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a new magic password';


    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->info("* Generate Magic Password!");

        $password = $this->_create_password();

        $mp = $this->_save_password($password);

        $this->_notifications($mp, $password);

        $this->_inform($mp, $password);
    }

    private function _create_password(): string
    {
        $this->info("\n** Create password");

        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890+*$-';
        $password = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 10; $i++)
        {
            $n = rand(0, $alphaLength);
            $password[] = $alphabet[$n];
        }
        $password = implode($password);

        $this->info("++ Generated");

        return $password;
    }
    private function _save_password(string $password): MagicPassword
    {
        $duration = $this->_get_duration();

        $expires_at = now()->addDays($duration);

        $magic_password = new MagicPassword([
            'password' => Hash::make($password),
            'expires_at' => $expires_at,
        ]);

        $magic_password->save();

        return $magic_password;
    }
    private function _notifications(MagicPassword $magic_password, string $password): void
    {
        $this->info("\n** Notifications");

        if( ! empty($this->option('notif') ) )
        {
            $email = Config::get('larauth.notifications.mail.to');

            $user = User::where('email', $email)->first();
            if( $user )
            {
                $this->info("++ Send to $user->email");
                $user->notify(new MagicPasswordCreatedNotification($magic_password, $password));
            }
            else
            {
                $this->info("!! User ( $email ) not found !");
                Log::error('[Larauth::GenerateMagicPassword::_notifications] LGMPn01 : Unable to find user with $email');
            }
        }
        else
        {
            $this->info("** add --notif for sending notif");
        }
    }
    private function _inform(MagicPassword $mp, string $password): void // TODO in trait
    {
        $this->info("\n** Info");
        $this->info("Magic password: $password");
        $this->info( "Available until " . $mp->expires_at);

        $this->info("\n");
    }
    private function _get_duration(): int
    {
        $default_duration = Config::get('larauth.default_duration', 10);

        $duration =  empty($this->option('duration')) ? $default_duration : $this->option('duration');

        return $duration;
    }
}

// refs :
