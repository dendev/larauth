<?php

namespace Dendev\Larauth\Console\Commands;

use Dendev\Larauth\Models\MagicPassword;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;

class StopMagicPasswords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larauth:stop_magic_passwords {--id= : stop a specific magic password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Stop magic passwords usage';


    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->info('* Stop Magic Password !');
        $this->_delete_magic_passwords();

        $this->_inform();
    }

    private function _delete_magic_passwords(): void
    {
        $this->info("\n** Delete");

        $is_specific =  ! empty($this->option('id'));

        if( $is_specific )
            MagicPassword::find($this->option('id'))->delete();
        else
            MagicPassword::whereNull('deleted_at')->delete();

        $this->info('++ done');
    }
    private function _inform(): void
    {
        $this->info("\n** Info");

        if( ! empty($this->option('id')) )
            $this->info('Magic password (' . $this->option('id') . ') was deleted');
        else
            $this->info('All magic password was deleted');

        $this->info("\n");
    }
}

// refs :
