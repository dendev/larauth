<?php

namespace Dendev\Larauth\Notifications;

use Dendev\Larauth\Models\MagicPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Slack\BlockKit\Blocks\ActionsBlock;
use Illuminate\Notifications\Slack\BlockKit\Blocks\ContextBlock;
use Illuminate\Notifications\Slack\BlockKit\Blocks\SectionBlock;
use Illuminate\Notifications\Slack\SlackMessage;
use Illuminate\Support\Facades\Config;

class MagicPasswordCreatedNotification extends Notification
{
    use Queueable;

    private MagicPassword $_magic_password;
    private string $_password;

    /**
     * Create a new notification instance.
     */
    public function __construct(MagicPassword $magic_password, string $password)
    {
        $this->_magic_password = $magic_password;
        $this->_password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        $targets = [];

        $destinations = Config::get('larauth.notifications');

        foreach( $destinations as $type => $destination )
            if( $destination['use_it'])
                $targets[] = $type;

        return $targets;
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject(__('larauth::notifications.magic_password_created_subject')) // TODO from package or host ?!
            ->greeting(__('larauth::notifications.magic_password_created_hello'))
            ->line(__('larauth::notifications.magic_password_created_info', [
                'magic_password_id' => $this->_magic_password->id,
                'magic_password_value' => $this->_password,
                'magic_password_expires_at' => $this->_magic_password->expires_at,
            ]));
            /*
            ->action($this->model->label,
                route('filament.resources.beers.edit', [
                    'record' => $this->model->id,
                ])
            )*/
    }

    public function toSlack(object $notifiable): SlackMessage
    {
        return (new SlackMessage)
            ->text('A new magic password is created!')
            //->headerBlock(__('larauth::notifications.magic_password_created_subject') )
            ->headerBlock('New Magic Password')
            ->contextBlock(function (ContextBlock $block) {
                $block->text($this->_password);
            })
            ->sectionBlock(function (SectionBlock $block) {
                $block->field("*Expires at:*\n" . $this->_magic_password->expires_at)->markdown();
                $block->field("*For App:*\n" . env('APP_NAME'))->markdown();
            })
            ->actionsBlock(function (ActionsBlock $block) {
                $block->button('Can be used here')->url(route('login'));
            });
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
