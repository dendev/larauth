<?php

namespace Dendev\Larauth\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MagicPasswordUser extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'magic_password_id'
    ];
}
