<?php

namespace Dendev\Larauth\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Dendev\Larauth\Models\MagicPassword
 * @property mixed $id
 * @property mixed $expires_at
 */
class MagicPassword extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'password',
        'expires_at'
    ];

    protected $casts = [
        'expires_at' => 'datetime'
    ];

    public function is_active(): bool
    {
        return now()->lte($this->expires_at);
    }
}
