<?php

namespace Dendev\Larauth\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Dendev\Larauth\Listeners\UserAuthAttempting;
use Illuminate\Auth\Events\Attempting;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        Attempting::class => [
           UserAuthAttempting::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
