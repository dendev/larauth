<?php

namespace Dendev\Larauth\Facades;

use Illuminate\Support\Facades\Facade;

class Larauth extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'larauth';
    }
}
